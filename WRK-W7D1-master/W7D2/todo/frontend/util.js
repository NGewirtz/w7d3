const Util = {};

Util.uniqueId = () => {
  return new Date().getTime();
};

export default Util;
