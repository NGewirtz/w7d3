import * as APIUtil from '../util/todo_api_util';
import { receiveErrors } from './error_actions';

export const RECEIVE_TODOS = 'RECEIVE_TODOS';
export const RECEIVE_TODO = 'RECEIVE_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';


export const removeTodo = (id) => {
  return { type: REMOVE_TODO, id };
};

export const receiveTodos = (todos) => {
  return { type: RECEIVE_TODOS, todos };
};

export const receiveTodo = (todo) => {
  return { type: RECEIVE_TODO, todo };
};

export const fetchTodos = () => {
  return function(dispatch) {
    return APIUtil.fetchTodos().then(todos => {
      return dispatch(receiveTodos(todos));
    });
  };
};

export const createTodo = (todo) => {
  return function(dispatch) {
    return APIUtil.createTodo(todo)
      .then(
        todo => dispatch(receiveTodo(todo)),
        err => dispatch(receiveErrors(err.responseJSON))
    );
  };
};

export const deleteTodo = (id) => {
  return function(dispatch) {
    return APIUtil.deleteTodo(id)
    .then(
      id => dispatch(removeTodo(id))
    );
  };
};
