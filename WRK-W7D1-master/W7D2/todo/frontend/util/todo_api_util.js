export const fetchTodos = () => {

  return $.ajax({
    method: 'GET',
    url: '/api/todo_items'
  });
};

export const createTodo = (todo) => {
  return $.ajax({
    method: 'POST',
    url: '/api/todo_items',
    data: todo
  });
};

export const deleteTodo = (id) => {
  return $.ajax({
      method: 'DELETE',
      url: `/api/todo_items/${id}`
  });
};
