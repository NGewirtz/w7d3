import { combineReducers } from 'redux';
import todos from './todos_reducer';
import errors from './error_reducer';
// import receiveTodo from '../actions/todo_actions';

const rootReducer = combineReducers({
  todos,
  errors,
});

export default rootReducer;
