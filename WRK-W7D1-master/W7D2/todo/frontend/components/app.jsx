// import Todo from
import React from 'react';
import ToDoListContainer from './todo_list/todo_list_container';

const App = (props) => {
  return (
    <div>
      <h1>App working?</h1>
      <ToDoListContainer />
    </div>
  );
};

export default App;
