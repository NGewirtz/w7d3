import React from 'react';
import { Component } from 'react';

class TodoForm extends Component {

  constructor (props) {
    super(props);
    this.state = { title: "", body: ""};
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    // console.log(this.props.receiveTodo(this.state));
    const todo = this.state;
    this.props.createTodo({todo}).then(
      () => this.setState({title: '', body: ''})
);
  }

  assignHandler(fieldname) {
    return (e) => {
      const newState = { [fieldname]: e.currentTarget.value };
      this.setState(newState);
    };
  }

  render() {

    const errors = this.props.errors.map((error, idx) => {
      return (
        <li key={idx}>{error}</li>
      );
    });

    return (
      <div>
        <ul>
          { errors }
        </ul>

        <form onSubmit={this.handleSubmit}>
          <label>Title
            <input
              type="text"
              placeholder="Title..."
              onChange={this.assignHandler("title")}
              value={this.state.title}
              />
          </label>
          <br />
          <label>Body
            <input
              type="text"
              placeholder="Details..."
              onChange={this.assignHandler("body")}
              value={this.state.body}
              />
          </label>
          <br />
          <button>Add to List</button>
        </form>
      </div>
    );
  }
}

export default TodoForm;
