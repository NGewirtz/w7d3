import React from 'react';
import { Component } from 'react';
import TodoForm from './todo_form';

class ToDoList extends Component {
  //props: props.todos (array of todo objects)
    // & props.receiveTodo (dispatch function receiveTodo(todo))
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchTodos();
  }

  render () {
    const todoListItems = this.props.todos.map((todo, idx) => {
      return (<TodoListItem key={idx} todo={todo} />);
    });

    return (
      <div>
        <TodoForm errors={ this.props.errors } createTodo={ this.props.createTodo }/>
        <ul>
          {todoListItems}
        </ul>
      </div>
    );
  }
}

const TodoListItem = (props) => {
  return (
  <li>
    {props.todo.title}
    <ul>
      <li>{props.todo.body}</li>
      <li>Done?: {props.todo.done ? "Yes" : "No"}</li>
    </ul>
  </li>);
};

export default ToDoList;
