const path = require("path");

module.exports = {
  // entry: "app/frontend/todo_redux.jsx",
  entry: path.join(__dirname, "frontend", "todo_redux.jsx"),
  output: {
    path: path.join(__dirname, "app", "assets", "javascripts"),
    // path: "app/assets/javascripts",
    // filename: "[name].bundle.js"
    filename: "[name].bundle.js"
  },
  module: {
    loaders: [
      {
        test: [/\.jsx?$/, /\.js?$/],
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        }
      }
    ]
  },
  devtool: 'source-map',
  resolve: {
    extensions: [".js", '.jsx', '*']
  }
};
