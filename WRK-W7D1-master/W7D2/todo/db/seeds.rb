# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
TodoItem.destroy_all
titles = ["wash house", "paint", "make soup", "take care of pet"]
bodies = ["pressure washer rental", "get good brush", "get lentils", "get bird seed"]
done = [true, false]
titles.each_with_index do |title, idx|
    TodoItem.new(title: title, body: bodies[idx], done: done.sample).save
end
