import { RECEIVE_TODOS, RECEIVE_TODO } from "../actions/todo_actions.js";


// reducers/todos_reducer.js
const initialState = {
  1: {
    id: 1,
    title: 'wash car',
    body: 'with soap',
    done: false
  },
  2: {
    id: 2,
    title: 'wash dog',
    body: 'with shampoo',
    done: true
  },
};


const uniqueId = () => {
  return new Date().getTime();
};


const todosReducer = (state = initialState, action) => {

  switch(action.type) {
    case RECEIVE_TODOS:
      let newState = {};
      action.todos.forEach((todo) => newState[todo.id] = todo);
      return newState;
    case RECEIVE_TODO:
      action.todo.id = action.todo.id || uniqueId();
      const newState2 = Object.assign({}, state, { [action.todo.id]: action.todo });
      return newState2;
    default:
      return state;
  }
};

export default todosReducer;
