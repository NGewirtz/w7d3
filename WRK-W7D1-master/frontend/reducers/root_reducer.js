import { combineReducers } from 'redux';
import todos from './todos_reducer';
// import receiveTodo from '../actions/todo_actions';

const rootReducer = combineReducers({
  todos,
});

export default rootReducer;
