import React from 'react';
import { Component } from 'react';
import TodoForm from './todo_form';

class ToDoList extends Component {
  //props: props.todos (array of todo objects)
    // & props.receiveTodo (dispatch function receiveTodo(todo))
  
  render () {

    const todoListItems = this.props.todos.map((todo, idx) => {
      return (<TodoListItem key={idx} todo={todo} />);
    });

    return (
      <div>
        <TodoForm receiveTodo={ this.props.receiveTodo }/>
        <ul>
          {todoListItems}
        </ul>
      </div>
    );
  }
}

const TodoListItem = (props) => {
  return (
  <li>
    {props.todo.title}
    <ul>
      <li>{props.todo.body}</li>
    </ul>
  </li>);
};

export default ToDoList;
