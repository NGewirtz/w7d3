import React from 'react';
import { Component } from 'react';
import Util from '../../../util.js';

class TodoForm extends Component {

  constructor (props) {
    super(props);
    this.state = { title: "", body: ""};
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log(this.props.receiveTodo(this.state));
  }

  assignHandler(fieldname) {
    return (e) => {
      const newState = { [fieldname]: e.currentTarget.value };
      this.setState(newState);
    };
  }



  render() {

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>Title
            <input
              type="text"
              onChange={this.assignHandler("title")}
              value={this.state.title}
              />
          </label>
          <br />
          <label>Body
            <input
              type="text"
              onChange={this.assignHandler("body")}
              value={this.state.body}
              />
          </label>
          <br />
          <button>Add to List</button>
        </form>
      </div>
    );
  }
}

export default TodoForm;
